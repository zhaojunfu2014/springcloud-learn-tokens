# springcloud-learn-tokens

#### 介绍
微服务的token示例代码：支持普通md5模式token、jwt、jwt结合AES加密

[代码说明](https://blog.csdn.net/u011177064/article/details/104822700)

![avatar](https://img-blog.csdnimg.cn/20200312201159532.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTExNzcwNjQ=,size_16,color_FFFFFF,t_70)

支持3种Token形式：

#### 1、MD5 Token

属于系统内部自定义一种Token规则，我这里只是简单地将用户的几个字段进行了拼接MD5，用户调用接口登录成功后，就把token生成并颁发给客户端。

验证时需要在数据库中查询token颁发记录，从而获取对应的用户信息

#### 2、Jwt

JSON Web Tokens，一种便于分布式架构中传输Token的一种规范，跟上述的Token串不同的是，它可以直接把用户信息也打包进去。所以这里面常常存的不只是一个令牌，而是直接包含了业务接口所需的用户信息、以及业务数据。Jwt默认不加密，只是进行签名验证，防止篡改，但是里面存放的数据是可见的（通过Base64URL 解码）。

验证时直接从Jwt中提取登录用户信息，不需要查询数据库

#### 3、Jwt+ AES

是基于第二种方案进行了AES对称加密，这样在传输过程中，里面的数据是不可见的。

验证时进行AES解密出原Jwt，直接从Jwt中提取登录用户信息，不需要查询数据库



