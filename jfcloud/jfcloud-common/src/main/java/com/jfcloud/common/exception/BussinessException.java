package com.jfcloud.common.exception;

public class BussinessException extends RuntimeException {

	public BussinessException(String string) {
		super(string);
	}

}
