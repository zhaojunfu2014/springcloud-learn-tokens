package com.jfcloud.common.http;


public class RespData{
	public static final int Success = 1;
	public static final int Error = 0;
	
	private int status;
	private Object datas = "";
	private Object meta = "";
	
	public RespData(Object datas) {
		this.datas = datas;
		this.status =Success;
	}
	
	
	public RespData(int status, Object datas) {
		this.status = status;
		if(status == Success){
			this.datas = datas;
		}else{
			this.meta = datas;
		}
	}
	
	
	
	public RespData(Exception e){
		this.status = Error;
		this.meta = e.getMessage();
	}

	public RespData() {
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Object getDatas() {
		return datas;
	}
	public void setDatas(Object datas) {
		this.datas = datas;
	}
	public Object getMeta() {
		return meta;
	}
	public void setMeta(Object meta) {
		this.meta = meta;
	}
	
	
	
}
