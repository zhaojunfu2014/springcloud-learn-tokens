package com.jfcloud.all.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.jfcloud.all.user.mapper")
@ComponentScan
public class JfcloudAllUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(JfcloudAllUserApplication.class, args);
    }
}
