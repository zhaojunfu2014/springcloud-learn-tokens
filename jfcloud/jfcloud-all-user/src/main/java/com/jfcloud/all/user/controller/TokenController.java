package com.jfcloud.all.user.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jfcloud.all.user.domain.param.GrantParams;
import com.jfcloud.all.user.service.TokenService;
import com.jfcloud.common.http.RespData;

@Controller
@RequestMapping("/token")
public class TokenController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenController.class);
	
	@Autowired
	@Qualifier(value = "SimpleTokenServiceImpl")
	private TokenService simpleTokenService;
	
	@Autowired
	@Qualifier(value = "JwtTokenServiceImpl")
	private TokenService jwtTokenService;
	
	@Autowired
	@Qualifier(value = "JwtAesServiceImpl")
	private TokenService jwtAesService;
	
	/**
	 * 简单token模式：step1.授权
	 * @param params
	 * @return
	 */
	@PostMapping(path = "/simple/grant")
	@ResponseBody
	public RespData grant(@RequestBody GrantParams params) {
		try {
			Object token = simpleTokenService.grant(params);
			return new RespData(token);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return new RespData(e);
		}
	}
	/**
	 * 简单token模式：step2.鉴定token有效性，并返回认证数据
	 * @param token
	 * @return
	 */
	@GetMapping(path = "/simple/validateAndGet")
	@ResponseBody
	public RespData validateAndGet(@RequestParam String token) {
		try {
			Object obj = simpleTokenService.validateAndGet(token);
			return new RespData(obj);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return new RespData(e);
		}
	}
	
	/**
	 * jwt token模式： step1.授权
	 * @param params
	 * @return
	 */
	@PostMapping(path = "/jwt/grant")
	@ResponseBody
	public RespData jwtGrant(@RequestBody GrantParams params) {
		try {
			Object token = jwtTokenService.grant(params);
			return new RespData(token);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return new RespData(e);
		}
	}
	
	/**
	 * jwt token模式：step2.鉴定token有效性，并返回认证数据
	 * @param token
	 * @return
	 */
	@RequestMapping(path = "/jwt/validateAndGet")
	@ResponseBody
	public RespData jwtValidateAndGet(@RequestParam(required = false) String token,HttpServletRequest request) {
		try {
			//支持以 POST GET URL参数方式校验token
			//【优先】支持将 token放入http的header里(参数名为：jwt_token)
			String headerToken = request.getHeader("jwt_token");
			if(StringUtils.isNotEmpty(headerToken)) {
				token = headerToken;
			}
			Assert.notNull(token,"token不能为空");
			Object obj = jwtTokenService.validateAndGet(token);
			return new RespData(obj);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return new RespData(e);
		}
	}
	
	
	/**
	 * jwt aes模式： step1.授权
	 * @param params
	 * @return
	 */
	@PostMapping(path = "/sjwt/grant")
	@ResponseBody
	public RespData sjwtGrant(@RequestBody GrantParams params) {
		try {
			Object token = jwtAesService.grant(params);
			return new RespData(token);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return new RespData(e);
		}
	}
	
	/**
	 * jwt aes模式：step2.鉴定token有效性，并返回认证数据
	 * @param token
	 * @return
	 */
	@RequestMapping(path = "/sjwt/validateAndGet")
	@ResponseBody
	public RespData sjwtValidateAndGet(@RequestParam(required = false) String token,HttpServletRequest request) {
		try {
			//支持以 POST GET URL参数方式校验token
			//【优先】支持将 token放入http的header里(参数名为：jwt_token)
			String headerToken = request.getHeader("jwt_token");
			if(StringUtils.isNotEmpty(headerToken)) {
				token = headerToken;
			}
			Assert.notNull(token,"token不能为空");
			Object obj = jwtAesService.validateAndGet(token);
			return new RespData(obj);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return new RespData(e);
		}
	}
}
