package com.jfcloud.all.user.persistent.impl;

import com.jfcloud.all.user.domain.JfcloudUser;
import com.jfcloud.all.user.mapper.JfcloudUserMapper;
import com.jfcloud.all.user.persistent.JfcloudUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaojunfu
 * @since 2020-03-08
 */
@Service
public class JfcloudUserServiceImpl extends ServiceImpl<JfcloudUserMapper, JfcloudUser> implements JfcloudUserService {

}
