package com.jfcloud.all.user.domain.param;

import java.io.Serializable;

import lombok.Data;

@Data
public class GrantParams implements Serializable{

	private String username;
	private String password;
	
}
