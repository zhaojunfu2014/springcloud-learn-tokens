package com.jfcloud.all.user.persistent;

import com.jfcloud.all.user.domain.JfcloudUserToken;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaojunfu
 * @since 2020-03-08
 */
public interface JfcloudUserTokenService extends IService<JfcloudUserToken> {

}
