package com.jfcloud.all.user.persistent.impl;

import com.jfcloud.all.user.domain.JfcloudUserJwt;
import com.jfcloud.all.user.mapper.JfcloudUserJwtMapper;
import com.jfcloud.all.user.persistent.JfcloudUserJwtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaojunfu
 * @since 2020-03-09
 */
@Service
public class JfcloudUserJwtServiceImpl extends ServiceImpl<JfcloudUserJwtMapper, JfcloudUserJwt> implements JfcloudUserJwtService {

}
