package com.jfcloud.all.user.mapper;

import com.jfcloud.all.user.domain.JfcloudUserJwt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaojunfu
 * @since 2020-03-09
 */
public interface JfcloudUserJwtMapper extends BaseMapper<JfcloudUserJwt> {

}
