package com.jfcloud.all.user.persistent.impl;

import com.jfcloud.all.user.domain.JfcloudUserToken;
import com.jfcloud.all.user.mapper.JfcloudUserTokenMapper;
import com.jfcloud.all.user.persistent.JfcloudUserTokenService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaojunfu
 * @since 2020-03-08
 */
@Service
public class JfcloudUserTokenServiceImpl extends ServiceImpl<JfcloudUserTokenMapper, JfcloudUserToken> implements JfcloudUserTokenService {

}
