package com.jfcloud.all.user.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jfcloud.all.user.domain.param.GrantParams;
import com.jfcloud.all.user.service.TokenService;
import com.jfcloud.common.utils.AESUtil;

/**
 * jwt + aes 加密
 * @author zhaojunfu
 *
 */
@Service(value = "JwtAesServiceImpl")
@Transactional
public class JwtAesServiceImpl implements TokenService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JwtAesServiceImpl.class);

	@Value(value = "${jfcloud.jwt.aes:zjf20200310}")
	private String aesKey;
	
	@Autowired
	@Qualifier(value = "JwtTokenServiceImpl")
	private TokenService jwtTokenService;
	
	
	@Override
	public Object grant(GrantParams params) throws Exception {
		Object jwt = jwtTokenService.grant(params);
		//加密后返回
		String jwtAES=AESUtil.encrypt(String.valueOf(jwt), aesKey);
		return jwtAES;
	}

	@Override
	public Object validateAndGet(Object token) {
		//先解密
		String tokenDecrypted = AESUtil.decrypt(String.valueOf(token), aesKey);
		return jwtTokenService.validateAndGet(tokenDecrypted);
	}

}
