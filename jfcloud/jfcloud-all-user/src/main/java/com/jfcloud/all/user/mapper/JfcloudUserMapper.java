package com.jfcloud.all.user.mapper;

import com.jfcloud.all.user.domain.JfcloudUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaojunfu
 * @since 2020-03-08
 */
public interface JfcloudUserMapper extends BaseMapper<JfcloudUser> {

}
