package com.jfcloud.all.user.service;

import com.jfcloud.all.user.domain.param.GrantParams;

/**
 * 微服务鉴权服务
 * @author zhaojunfu
 *
 */
public interface TokenService {
	
	/**
	 * 认证接口<br>
	 * 认证通过后返回token对象
	 * @param params 参数列表
	 * @return
	 * @throws Exception 
	 */
	Object grant(GrantParams params) throws Exception;
	
	
	/**
	 * 验证token有效性,并提取用户信息
	 * @param token
	 * @return
	 */
	Object validateAndGet(Object token);
}
