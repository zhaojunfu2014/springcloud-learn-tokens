
package com.jfcloud.all.user.test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jfcloud.all.user.JfcloudAllUserApplication;
import com.jfcloud.all.user.domain.JfcloudUser;
import com.jfcloud.all.user.persistent.JfcloudUserService;

@RunWith(SpringRunner.class)
//启动Spring
@SpringBootTest(classes=JfcloudAllUserApplication.class)
public class JfcloudUserServiceTest {

    @Autowired
    private JfcloudUserService jfcloudUserService;

    @Test
    public void save() throws Exception {
    	JfcloudUser  entity = new JfcloudUser();
    	entity.setUsername("hw");
    	entity.setAccount(new BigDecimal(500));
    	entity.setRealname("何婉");
    	entity.setOrderNum(0l);
    	entity.setBuyNum(0l);
    	entity.setCreateTime(LocalDateTime.now());
    	
    	jfcloudUserService.save(entity);
    	
    	
    }
}